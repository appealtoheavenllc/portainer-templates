# portainer-templates



Use the following Docker commands to deploy Portainer:

`docker volume create portainer_data`
`docker run -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer`
 docker run -d -p 9000:9000 --name portainer --restart always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer -H unix:///var/run/docker.sock										
You'll just need to access the port 9000 of the Docker engine where portainer is running using your browser.

Note: the -v /var/run/docker.sock:/var/run/docker.sock option can be used in Linux environments only.
